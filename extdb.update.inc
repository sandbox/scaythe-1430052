<?php
include_once 'extdb.utils.inc';

function extdb_node_update($node) {
  $importing = variable_get("extdb_importing", array());
  if (isset($importing[$node->type])) {
    return;
  }

  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_table')
        ->fieldCondition('extdb_content_type', 'value', $node->type, '=');
  $results = $query->execute();
  
  if (!empty($results)) {
    $links = node_load_multiple(array_keys($results['node']));
    
    foreach($links as $table) {
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'extdb_link_column')
            ->fieldCondition('extdb_table_link', 'nid', $table->nid, '=')
            ->fieldCondition('extdb_is_key', 'value', 1, '=');
      $results = $query->execute();
      if (!empty($results)) {
        $keys = node_load_multiple(array_keys($results['node']));
      }
      
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'extdb_link_column')
            ->fieldCondition('extdb_table_link', 'nid', $table->nid, '=')
            ->fieldCondition('extdb_is_key', 'value', 1, '!=');
      $results = $query->execute();
      
      if (!empty($results)) {
        $fields = node_load_multiple(array_keys($results['node']));
        
        $db_fields = array();
        foreach($fields as $field) {
          $db_fields[_field_get_item($field, 'extdb_column')] = _field_get_item($node, _field_get_item($field, 'extdb_field'));
        }
        
        db_set_active(_field_get_item($table, 'extdb_database'));
        $query = db_update(_field_get_item($table, 'extdb_table'));
        foreach($keys as $key) {
          $query = $query->condition(_field_get_item($key, 'extdb_column'), _field_get_item($node, _field_get_item($key, 'extdb_field')));
        }
        $query->fields($db_fields)
              ->execute();
        db_set_active();
      }
    }
  }
}

//function extdb_node_delete($node) {
//  $importing = variable_get("extdb_importing", array());
//  if (isset($importing[$node->type])) {
//    return;
//  }
//  
//  
//}
