<?php
include_once 'extdb.import.inc';

function extdb_xmlrpc() {
  $methods[] =  array(
    'extdb.import_type',
    '_extdb_import_type',
    array('int', 'string'),
    t('Tells drupal to import a type from its external database.')
  );
  $methods[] =  array(
    'extdb.test',
    '_extdb_test',
    array('string', 'string'),
    t('Testing.')
  );

  return $methods;
}

function _extdb_test($string) {
  return "testing : $string";
}

function _extdb_import_type($content_type) {
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_table')
        ->fieldCondition('extdb_content_type', 'value', $content_type, '=');
  $results = $query->execute();
  $links = node_load_multiple(array_keys($results['node']));
  
  $i = 0;
  foreach($links as $link) {
    if (_extdb_import_table($link->nid)) {
      $i += 1;
    }
  }
  return $i;
}
