<?php

// Form - import
function extdb_import_form($form, &$form_state) {
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_table');
  $results = $query->execute();
  
  $nodes = node_load_multiple(array_keys($results['node']));
  $form_state['extdb']['nodes'] = $nodes;
  
  foreach($nodes as $node) {
    $form["extdb_type_" . $node->nid] = array(
      '#type' => 'checkbox',
      '#title' => $node->title,
    );
  }
  
  $form['extdb_import_button'] = array(
    '#type' => 'submit',
    '#value' => t('Import'),
  );
  
  return $form;
}

function extdb_import_form_submit($form, &$form_state) {
  $nodes = $form_state['extdb']['nodes'];
  //$link_lists = array();
  foreach($form_state['values'] as $key => $value) {
    if (strpos($key, 'extdb_type_') === 0 && $value == 1) {
      $node = $nodes[intval(substr($key, 11))];
      $order = _field_get_item($node, 'extdb_order');
      if (!isset($links[$order])) {
        //$link_lists[$order] = array();
      }
      $link_lists[$order][] = $node;
      
    }
  }
  ksort($link_lists);
  foreach($link_lists as $link_list) {
    foreach($link_list as $link) {
      _extdb_import_table($link);
    }
  }
  
  $form_state['rebuild'] = TRUE;
}

function _extdb_import_table($link) {
  $content_type = _field_get_item($link, 'extdb_content_type');
    
  // getting the fields that are key
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_column')
        ->fieldCondition('extdb_table_link', 'nid', $link->nid, '=')
        ->fieldCondition('extdb_is_key', 'value', 1, '=');
  $results = $query->execute();
  $key_fields = node_load_multiple(array_keys($results['node']));

  // getting all the other fields
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_column')
        ->fieldCondition('extdb_table_link', 'nid', $link->nid, '=')
        ->fieldCondition('extdb_is_key', 'value', 1, '!=');
  $results = $query->execute();
  $fields = array();
  if (!empty($results)) {
    $fields = node_load_multiple(array_keys($results['node']));
  }
  
  
  $importing = variable_get("extdb_importing", array());
  $importing += array($content_type => TRUE);
  variable_set("extdb_importing", $importing);
  
  // getting all the entries in the external database
  db_set_active(_field_get_item($link, 'extdb_database'));
  $db_query = db_select(_field_get_item($link, 'extdb_table'), 't')
    ->fields('t')
    ->execute();
  
  while($entry = $db_query->fetch(PDO::FETCH_ASSOC)) {
    db_set_active();
    set_time_limit(30);
    // checking whether the entry already exists in the local database
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', $content_type);
    foreach($key_fields as $key_field) {
      $query->fieldCondition(_field_get_item($key_field, 'extdb_field'), 'value', $entry[strtolower(_field_get_item($key_field, 'extdb_column'))], '=');
    }
    $results = $query->execute();
    if (empty($results) && _field_get_item($link, 'extdb_order') == 0) {
      // insert new node in local database
      $node = new stdClass();
      $node->type = $content_type;
      $node->language = 'und';
      node_object_prepare($node);
      _fill_fields($node, $key_fields, $entry);
    } elseif (count($results) == 1) {
      // update node in local database
      $node = node_load(current(array_keys($results['node'])));
    } else {
      // error, multiple entries with same key
    }
    if (isset($node)) {
      _fill_fields($node, $fields, $entry);
      node_save($node);
      unset($node);
    }
    db_set_active(_field_get_item($link, 'extdb_database'));
  }
  db_set_active();
  
  $importing = variable_get("extdb_importing", array());
  unset($importing[$content_type]);
  variable_set("extdb_importing", $importing);
  
  return TRUE;
}

function _fill_fields($node, $fields, $source) {
  foreach($fields as $field) {
    $field_name = _field_get_item($field, 'extdb_field');
    _field_set_item($node, $field_name, $source[strtolower(_field_get_item($field, 'extdb_column'))]);
  }
}
