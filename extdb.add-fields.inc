<?php
include_once 'extdb.utils.inc';

// Form - Add fields from table
function extdb_add_fields_form($form, &$form_state, $type = null) {
  if ($type == null) return;
  
  if (empty($form_state['extdb'])) {
    $form_state['extdb'] = array();
    $form_state['extdb']['step'] = 'db';
    $form_state['extdb']['type'] = $type->type;
    $form_state['extdb']['db_name'] = '';
    $form_state['extdb']['table_name'] = '';
    $form_state['extdb']['link_name'] = '';
  }

  $form['description'] = array(
    '#type' => 'item',
    '#title' => 'Step @n / 4',
  );
  
  if ($form_state['extdb']['db_name'] != '') {
    $form['description_db'] = array(
      '#type' => 'item',
      '#title' => t("Selected database : @db", array("@db" => $form_state['extdb']['db_name'])),
    );
  }
  if ($form_state['extdb']['table_name'] != '') {
    $form['description_table'] = array(
      '#type' => 'item',
      '#title' => t("Selected table : @table", array("@table" => $form_state['extdb']['table_name'])),
    );
  }
  if ($form_state['extdb']['link_name'] != '') {
    $form['description_link'] = array(
      '#type' => 'item',
      '#title' => t(($form_state['extdb']['link_id'] == 0 ? "New" : "Selected") . " link : @link", array("@link" => $form_state['extdb']['link_name'])),
    );
  }

  switch ($form_state['extdb']['step']) {
    case 'db':
      return extdb_add_fields_db_form($form, $form_state);
    case 'table':
      return extdb_add_fields_table_form($form, $form_state);
    case 'link':
      return extdb_add_fields_link_form($form, $form_state);
    case 'fields':
      return extdb_add_fields_fields_form($form, $form_state);
  }
}

function extdb_add_fields_db_form($form, &$form_state) {
  $form['description']['#title'] = t($form['description']['#title'], array('@n' => '1'));

  $form['extdb_db'] = array(
    '#type' => 'textfield',
    '#title' => t('External database identifier'),
    '#description' => t('The identifier of the database as defined in the settings.php file.'),
    '#default_value' => empty($form_state['extdb']['db']) ? '' : $form_state['extdb']['db'],
  );
  $form['extdb_db_button'] = array(
    '#type' => 'submit',
    '#value' => t('Continue'),
    '#validate' => array('extdb_add_fields_db_form_validate'),
    '#submit' => array('extdb_add_fields_db_form_submit'),
  );
  return $form;
}

function extdb_add_fields_db_form_validate($form, &$form_state) {
  db_set_active($form_state['values']['extdb_db']);

  if (db_set_active() == "default") {
    form_set_error('extdb_db', t('Please enter a valid key for an external database.'));
  }
}

function extdb_add_fields_db_form_submit($form, &$form_state) {
  if (empty($form_state['extdb']['db']) || $form_state['extdb']['db'] != $form_state['values']['extdb_db']) {
    $form_state['extdb']['table'] = '';
  }
  $form_state['extdb']['db'] = $form_state['values']['extdb_db'];
  $form_state['extdb']['db_name'] = $form_state['extdb']['db'];
  db_set_active($form_state['extdb']['db']);

  $tables = db_query('SHOW TABLES');

  $form_state['extdb']['tables'] = $tables->fetchAll(PDO::FETCH_COLUMN);

  db_set_active();

  $form_state['extdb']['step'] = 'table';
  $form_state['rebuild'] = TRUE;
}

function extdb_add_fields_table_form($form, &$form_state) {
  $form['description']['#title'] = t($form['description']['#title'], array('@n' => '2'));

  $form['extdb_table'] = array(
    '#type' => 'select',
    '#title' => t('Table in database @db', array('@db' => $form_state['extdb']['db'])),
    '#description' => t('The table to get the fields from.'),
    '#options' => $form_state['extdb']['tables'],
    '#default_value' => $form_state['extdb']['table'],
  );
  
  $form['extdb_table_back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('extdb_add_fields_table_form_back'),
  );
  $form['extdb_table_button'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed'),
    '#submit' => array('extdb_add_fields_table_form_submit'),
  );
  return $form;
}

function extdb_add_fields_table_form_submit($form, &$form_state) {
  if ((empty($form_state['extdb']['table']) && $form_state['extdb']['table'] !== '0') || $form_state['extdb']['table'] != $form_state['values']['extdb_table']) {
    $form_state['extdb']['link_id'] = 0;
    $form_state['extdb']['link'] = $form_state['extdb']['db'] . " " . $form_state['extdb']['tables'][$form_state['values']['extdb_table']] . " - " . $form_state['extdb']['type'];
    $form_state['extdb']['link_name'] = '';
    $form_state['extdb']['order'] = 0;
  }
  $form_state['extdb']['table'] = $form_state['values']['extdb_table'];
  $form_state['extdb']['table_name'] = $form_state['extdb']['tables'][$form_state['values']['extdb_table']];
  
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_table')
        ->fieldCondition('extdb_database', 'value', $form_state['extdb']['db'], '=')
        ->fieldCondition('extdb_table', 'value', $form_state['extdb']['table_name'], '=')
        ->fieldCondition('extdb_content_type', 'value', $form_state['extdb']['type'], '=');
  $results = $query->execute();
  
  $options = array(0 => t("New link"));
  if (!empty($results)) {
    $links = node_load_multiple(array_keys($results['node']));
    foreach($links as $link) {
      $options[$link->nid] = $link->title;
    }
  }
  $form_state['extdb']['links'] = $options;

  $form_state['rebuild'] = TRUE;
  $form_state['extdb']['step'] = 'link';
}

function extdb_add_fields_table_form_back($form, &$form_state) {
  $form_state['extdb']['db_name'] = '';

  $form_state['rebuild'] = TRUE;
  $form_state['extdb']['step'] = 'db';
}

function extdb_add_fields_link_form($form, &$form_state) {
  $form['description']['#title'] = t($form['description']['#title'], array('@n' => '3'));
  
  $form["extdb_table_link"] = array(
    '#type' => 'fieldset',
    '#title' => t("Table-type link"),
  );
  
  $form["extdb_table_link"]['extdb_link'] = array(
    '#type' => 'select',
    '#title' => t("Table-type link"),
    '#options' => $form_state['extdb']['links'],
    '#default_value' => $form_state['extdb']['link_id'],
    '#ajax' => array(
      'callback' => 'ajax_hide_new_link_callback',
      'wrapper' => "container-new",
      'effect' => "slide",
    ),
  );
  
  $form['extdb_table_link']["container"] = array(
    '#type' => 'container',
    '#id' => "container-new",
  );
  
  if ((!isset($form_state['values']["extdb_link"]) || $form_state['values']["extdb_link"] == 0) && (isset($form_state['values']["extdb_link"]) || empty($form_state['extdb']['link_id']) || $form_state['extdb']['link_id'] == 0)) {
    $form["extdb_table_link"]["container"]['extdb_name'] = array(
      '#type' => 'textfield',
      '#title' => t("Name"),
      '#default_value' => $form_state['extdb']['link'],
      '#required' => TRUE,
    );
    $form["extdb_table_link"]["container"]['extdb_order'] = array(
      '#type' => 'textfield',
      '#title' => t("Order"),
      '#default_value' => $form_state['extdb']['order'],
      '#required' => TRUE,
    );
  }
  
  $form['extdb_link_back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('extdb_add_fields_link_form_back'),
  );
  $form['extdb_link_button'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed'),
    '#validate' => array('extdb_add_fields_link_form_validate'),
    '#submit' => array('extdb_add_fields_link_form_submit'),
  );
  return $form;
}

function ajax_hide_new_link_callback($form, $form_state) {
  return $form["extdb_table_link"]["container"];
}

function extdb_add_fields_link_form_back($form, &$form_state) {
  $form_state['extdb']['table_name'] = '';
  
  $form_state['rebuild'] = TRUE;
  $form_state['extdb']['step'] = 'table';
}

function extdb_add_fields_link_form_validate($form, &$form_state) {
  if (isset($form_state['values']['extdb_order'])) {
    if (!is_numeric($order = trim($form_state['values']['extdb_order'])) ) {
      form_set_error('extdb_order', t('Must be a number.'));
    } elseif ($order < 0) {
      form_set_error('extdb_order', t('Must be greater than or equal to 0.'));
    } elseif ($order == 0) {
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'extdb_link_table')
            ->fieldCondition('extdb_content_type', 'value', $form_state['extdb']['type'], '=');
      $results = $query->execute();
      
      $links = array();
      if (!empty($results)) {
        $links = node_load_multiple(array_keys($results['node']));
      }
      foreach($links as $link) {
        if (_field_get_item($link, 'extdb_order') == 0) {
          form_set_error('extdb_order', t('There must be only one link with order 0 for a content type.'));
          break;
        }
      }
    } else {
      $query = new EntityFieldQuery;
      $query->entityCondition('entity_type', 'node')
            ->entityCondition('bundle', 'extdb_link_table')
            ->fieldCondition('extdb_content_type', 'value', $form_state['extdb']['type'], '=');
      $results = $query->execute();
      if (empty($results)) {
        form_set_error('extdb_order', t('The first link for a content type must have order 0.'));
      }
    }
  }
}

function extdb_add_fields_link_form_submit($form, &$form_state) {
  $form_state['extdb']['link_id'] = $form_state['values']['extdb_link'];
  if ($form_state['extdb']['link_id'] == 0) {
    $form_state['extdb']['link'] = $form_state['values']['extdb_name'];
  } else {
    $form_state['extdb']['link'] = $form_state['extdb']['links'][$form_state['extdb']['link_id']];
  }
  $form_state['extdb']['link_name'] = $form_state['extdb']['link'];
  if (isset($form_state['values']['extdb_order'])) {
    $form_state['extdb']['order'] = $form_state['values']['extdb_order'];
  }
  
  $query = new EntityFieldQuery;
  $query->entityCondition('entity_type', 'node')
        ->entityCondition('bundle', 'extdb_link_column')
        ->fieldCondition('extdb_table_link', 'nid', $form_state['extdb']['link_id'], '=');
  $results = $query->execute();
  $existing_same = array();
  if (!empty($results)) {
    $existing_same = node_load_multiple(array_keys($results['node']));
  }
  $form_state['extdb']['es'] = array();
  foreach($existing_same as $es) {
    $form_state['extdb']['es'][_field_get_item($es, "extdb_column")] = $es;
  }
  
  $links = $form_state['extdb']['links'];
  
  unset($links[0]);
  unset($links[$form_state['extdb']['link_id']]);
  $existing_other = array();
  foreach($links as $nid => $link) {
    $query = new EntityFieldQuery;
    $query->entityCondition('entity_type', 'node')
          ->entityCondition('bundle', 'extdb_link_column')
          ->fieldCondition('extdb_table_link', 'nid', $nid, '=');
    $results = $query->execute();
    if (!empty($results)) {
      $existing_other += node_load_multiple(array_keys($results['node']));
    }
  }
  $form_state['extdb']['eo'] = array();
  foreach($existing_other as $eo) {
    $form_state['extdb']['eo'][_field_get_item($eo, "extdb_column")] = $eo;
  }
  
  db_set_active($form_state['extdb']['db']);
  $fields = db_query('SHOW COLUMNS FROM ' . $form_state['extdb']['table_name']);

  $f = array();
  $form_state['extdb']['fields'] = array();
  foreach($fields->fetchAll(PDO::FETCH_ASSOC) as $weight => $fields) {
    $form_state['extdb']['fields'][$weight] = array(
      'label' => $fields['field'],
      'name' => $fields['field'],
      'orig_type' => $fields['type'],
      'required' => $fields['null'] == "NO" && strpos($fields['extra'], "auto_increment") === FALSE,
      'default' => $fields['default'],
      'weight' => $weight,
      'key' => $fields['key'] == "PRI",
      'autoinc' => strpos($fields['extra'], "auto_increment") !== FALSE,
    );
  }
  db_set_active();
  
  $form_state['rebuild'] = TRUE;
  $form_state['extdb']['step'] = 'fields';
}


function extdb_add_fields_fields_form($form, &$form_state) {
  $form['description']['#title'] = t($form['description']['#title'], array('@n' => '4'));
  
  if (!empty($form_state['extdb']['fields'])) {
    $form['fields'] = array(
      '#type' => 'fieldset',
      '#title' => t("Fields"),
    );
  }
  
  $type_options = array();
  foreach (field_info_field_types() as $name => $field_type) {
    if (empty($field_type['no_ui'])) {
      $type_options[$name] = $field_type['label'];
    }
  }
  asort($type_options);

  foreach($form_state['extdb']['fields'] as $weight => $fields) {
    if (!empty($form_state['extdb']['es'][$fields['name']])) {
      $form['fields']['description_' . $fields['name']] = array(
        '#type' => 'item',
        '#title' => t("@field_name already linked in same table-type link.", array('@field_name' => $fields['name'])),
      );
    } elseif (!empty($form_state['extdb']['eo'][$fields['name']])) {
      $form['fields']['description_' . $fields['name']] = array(
        '#type' => 'item',
        '#title' => t("@field_name already linked in another table-type link.", array('@field_name' => $fields['name'])),
      );
    } else {
      $form['fields']["extdb_field_$weight"] = array(
        '#type' => 'fieldset',
        '#title' => $fields['name'],
        '#collapsible' => TRUE,
      );
      $form['fields']["extdb_field_$weight"]["add_$weight"] = array(
        '#type' => 'checkbox',
        '#title' => t('Add field'),
        '#ajax' => array(
          'callback' => 'ajax_replace_fieldset_callback',
          'wrapper' => "container-$weight",
          'effect' => "slide",
        ),
      );
      $form['fields']["extdb_field_$weight"]["container"] = array(
        '#type' => 'container',
        '#id' => "container-$weight",
      );
      if (!empty($form_state['values']["add_$weight"])) {
        $form['fields']["extdb_field_$weight"]["container"]["field"] = array(
          '#type' => 'fieldset',
          '#title' => t('@type field', array('@type' => $form_state['extdb']['type'])),
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_label_$weight"] = array(
          '#type' => 'textfield',
          '#title' => t('Label'),
          '#default_value' => str_replace('_', ' ', $fields['label']),
          '#size' => 40,
          '#maxlength' => 60,
          '#required' => TRUE,
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_name_$weight"] = array(
          '#type' => 'textfield',
          '#title' => t('Field name'),
          '#default_value' => "field_" . strtolower($fields['label']),
          '#size' => 40,
          '#maxlength' => 60,
          '#required' => TRUE,
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_type_$weight"] = array(
          '#type' => 'select',
          '#title' => t("Type"),
          '#options' => $type_options,
          '#required' => TRUE,
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_orig_type_$weight"] = array(
          '#type' => 'textfield',
          '#title' => t('Original type'),
          '#value' => $fields['orig_type'],
          '#disabled' => TRUE,
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_default_$weight"] = array(
          '#type' => 'textfield',
          '#title' => t('Default value'),
          '#value' => $fields['default'],
        );
        $form['fields']["extdb_field_$weight"]["container"]["field"]["field_required_$weight"] = array(
          '#type' => 'checkbox',
          '#title' => t('Required'),
          '#value' => $fields['required'],
        );
        $form['fields']["extdb_field_$weight"]["container"]["link"] = array(
          '#type' => 'fieldset',
          '#title' => t('Column-field link entity'),
        );
        $form['fields']["extdb_field_$weight"]["container"]["link"]["key_$weight"] = array(
          '#type' => 'checkbox',
          '#title' => t('Primary key'),
          '#default_value' => $fields['key'],
        );
        $form['fields']["extdb_field_$weight"]["container"]["link"]["link_label_$weight"] = array(
          '#type' => 'textfield',
          '#title' => t('Label'),
          '#default_value' => $form_state['extdb']['db'] . " " . $form_state['extdb']['table_name'] . " " . strtolower($fields['label']) . " - " . $form_state['extdb']['type'],
          '#size' => 100,
          '#maxlength' => 250,
          '#required' => TRUE,
        );
      }
    }
  }
  $form['extdb_fields_back'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
    '#submit' => array('extdb_add_fields_fields_form_back'),
  );
  $form['extdb_fields_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Commit'),
    '#validate' => array('extdb_add_fields_fields_form_validate'),
    '#submit' => array('extdb_add_fields_fields_form_submit'),
  );
  return $form;
}

function ajax_replace_fieldset_callback($form, $form_state) {
  return $form['fields'][str_replace("add_", "extdb_field_", $form_state['triggering_element']['#name'])]["container"];
}

function extdb_add_fields_fields_form_back($form, &$form_state) {
  $form_state['rebuild'] = TRUE;
  $form_state['extdb']['step'] = 'link';
  $form_state['extdb']['link_name'] = '';
  if ($form_state['extdb']['link_id'] != 0) {
    $form_state['extdb']['link'] = '';
  }
}

function extdb_add_fields_fields_form_validate($form, &$form_state) {
  
}

function extdb_add_fields_fields_form_submit($form, &$form_state) {
  // create table-type link if new
  $link_id = $form_state['extdb']['link_id'];
  if ($link_id == 0) {
    $node = new stdClass();
    $node->title = $form_state['extdb']['link'];
    $node->type = 'extdb_link_table';
    $node->language = 'und';
    node_object_prepare($node);
    _field_set_item($node, 'extdb_database', $form_state['extdb']['db']);
    _field_set_item($node, 'extdb_table', $form_state['extdb']['table_name']);
    _field_set_item($node, 'extdb_content_type', $form_state['extdb']['type']);
    _field_set_item($node, 'extdb_order', $form_state['extdb']['order']);
    node_save($node);
    $link_id = $node->nid;
  }
  
  // foreach added field
  foreach($form_state['extdb']['fields'] as $pos => $field) {
    if(isset($form_state['values']["add_$pos"]) && $form_state['values']["add_$pos"]) {
      $field_exists = field_read_field($form_state['values']["field_name_$pos"], array('include_inactive' => TRUE));
      if ($form_state['values']["field_name_$pos"] != 'title' && empty($field_exists)) {
        // creating field
        $field_tmp = array(
          'field_name'  => $form_state['values']["field_name_$pos"],
          'cardinality' => 1,
          'type'        => $form_state['values']["field_type_$pos"],
        );
        field_create_field($field_tmp);
      }
      
      $instance_exists = field_read_instance('node', $form_state['values']["field_name_$pos"], $form_state['extdb']["type"], array('include_inactive' => TRUE));
      if ($form_state['values']["field_name_$pos"] != 'title' && empty($instance_exists)) {
        // adding field instance to content type
        $instance = array(
          'bundle'      => $form_state['extdb']["type"],
          'field_name'  => $form_state['values']["field_name_$pos"],
          'label'       => $form_state['values']["field_label_$pos"],
          'required'    => $form_state['values']["field_required_$pos"],
          'default'     => array(array('value' => $form_state['values']["field_default_$pos"])),
          'entity_type' => 'node',
        );
        field_create_instance($instance);
      }
      
      // creating column-field link
      $node = new stdClass();
      $node->title = $form_state['values']["link_label_$pos"];
      $node->type = 'extdb_link_column';
      $node->language = 'und';
      node_object_prepare($node);
      _field_set_item($node, 'extdb_column', $field['name']);
      _field_set_item($node, 'extdb_field', $form_state['values']["field_name_$pos"]);
      _field_set_item($node, 'extdb_is_key', $form_state['values']["key_$pos"]);
      _field_set_item($node, 'extdb_table_link', $link_id, 'nid');
      node_save($node);
    }
  }
}
