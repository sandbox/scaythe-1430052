<?php

function _field_get_item($entity, $field_name, $type = "value", $langcode = NULL) {
  if ($field_name == 'title') {
    return $entity->title;
  }
  $result = current(field_get_items('node', $entity, $field_name, $langcode));
  return $result[$type];
}

function _field_set_item(&$entity, $field_name, $value, $type = "value", $langcode = NULL) {
  if ($langcode == NULL) $langcode = $entity->language;
  if ($field_name == 'title') {
    $entity->title = substr($value, 0, 254);
  } else {
    if (!empty($entity->$field_name)) {
      $field_array = $entity->$field_name;
    }
    $field_array[$langcode][0][$type] = $value;
    $entity->$field_name = $field_array;
  }
}
